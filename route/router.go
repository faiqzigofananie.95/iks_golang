package route

import (
	"iks_golang/api"
	// "iks_golang/config"
	// "iks_golang/model"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
)

func Init() *echo.Echo {
	e := echo.New()
	// configuration := config.GetConfig()


	// Middleware
	e.Use(loggingMiddleware)
	e.Use(middleware.Recover())

	// e.GET("/", api.Home)
	r := e.Group("/api")

	e.GET("/swagger/*", echoSwagger.WrapHandler)
	e.POST("/login", api.Login)
	e.POST("/register", api.Register)
	e.POST("/update-user", api.UpdateUser)
	e.POST("/delete-user", api.DeleteUser)

	// config := middleware.JWTConfig{
	// 	Claims:     &model.Claims{},
	// 	SigningKey: []byte(configuration.SECRET_KEY),
	// }
	// r.Use(middleware.JWTWithConfig(config))
	
	r.GET("/", api.Auth)

	r.POST("/survei", api.PostSurvei)
	r.GET("/survei", api.GetSurvei)

	//iks inti
	r.POST("/iks-inti", api.PostIksInti)
	r.GET("/iks-inti", api.GetIksInti)

	//provinsi
	r.POST("/provinsi", api.PostProvinsi)
	r.GET("/provinsi", api.GetProvinsi)

	//kota
	r.POST("/kota", api.PostKota)
	r.GET("/kota", api.GetKota)

	//kecamatan
	r.POST("/kecamatan", api.PostKecamatan)
	r.GET("/kecamatan", api.GetKecamatan)

	//kelurahan
	r.POST("/kelurahan", api.PostKelurahan)
	r.GET("/kelurahan", api.GetKelurahan)

	//ref_puskesmas
	r.POST("/ref-puskesmas", api.PostRefPuskesmas)
	r.GET("/ref-puskesmas", api.GetRefPuskesmas)

	return e
}

func loggingMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func (c echo.Context) error {
		start := time.Now()
		res := next(c)

		log.WithFields(log.Fields{
			"method": c.Request().Method,
			"path": c.Path(),
			"status": 200,
			"latency_ns": time.Since(start).Nanoseconds(),
		}).Info("")
		

		return res
	}
}
