package api

import (
	"net/http"
	"iks_golang/db"
	"iks_golang/model"
	"encoding/json"
	// "fmt"
	"github.com/davecgh/go-spew/spew"

	"github.com/labstack/echo/v4"
)


// PostKota godoc
// @Summary Input data to the system
// @Tags Input
// @Accept json 
// @Param JSON body model.Kota true "JSON RAW"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Kota} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/kota [post]
func PostKota(c echo.Context) (err error) {
	db := db.DbManager()
	kota := new(model.Kota)
	dataKota := model.Kota{} 
	dataProvinsi := model.Provinsi{} 
	
	//decode json to struct
	err = json.NewDecoder(c.Request().Body).Decode(&kota)
	spew.Dump(err)
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"data":    err,
			"message": err.Error(),
		})
	}

	//start check kota_id if exist is error
	findKotaId := db.Debug().Where("kota_id = ?", kota.KotaId).First(&dataKota)
	if findKotaId.RowsAffected != 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "kota_id is already exist",
		})
	}
	//stop check kota_id if exist is error

	//start check provinsi_id if not exist is error
	findProvinsiId := db.Debug().Where("provinsi_id = ?", kota.ProvinsiId).First(&dataProvinsi)
	if findProvinsiId.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "provinsi_id is not exist",
		})
	}
	//stop check provinsi_id if not exist is error

	//start insert data to table kota
	if createKota := db.Debug().Create(&kota); createKota.Error != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Failed to insert data to kota table",
		})
	} 
	//stop insert data to table kota

	return c.JSON(http.StatusOK, &map[string]interface{}{
		"error":   false,
		"message": "success",
		"data": kota,
		"status": http.StatusOK,

	})
}

// GetKota godoc
// @Summary GET data from the system
// @Tags Input
// @Accept json 
// @Param kota_id query string false "kota_id to find related record"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Kota} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/kota [get]
func GetKota(c echo.Context) error {
	db := db.DbManager()
	kota := new(model.Kota)

	kota_id := c.QueryParam("kota_id")

	//get data from kota table
	dataKota := db.Debug().Where("kota_id = ?", kota_id).First(&kota)
	if dataKota.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
      "message": "Success",
	  "data": dataKota,
	  "error": false,
	  "status": http.StatusOK,
	})
}