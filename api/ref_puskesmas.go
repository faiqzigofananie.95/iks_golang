package api

import (
	"net/http"
	"iks_golang/db"
	"iks_golang/model"
	"encoding/json"
	// "fmt"
	"github.com/davecgh/go-spew/spew"

	"github.com/labstack/echo/v4"
)


// PostRefPuskesmas godoc
// @Summary Input data to the system
// @Tags Input
// @Accept json 
// @Param JSON body model.RefPuskesmas true "JSON RAW"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.RefPuskesmas} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/ref-puskesmas [post]
func PostRefPuskesmas(c echo.Context) (err error) {
	db := db.DbManager()
	refPuskesmas := new(model.RefPuskesmas)
	dataRefPuskesmas := model.RefPuskesmas{} 
	dataKecamatan := model.Kecamatan{} 
	
	//decode json to struct
	err = json.NewDecoder(c.Request().Body).Decode(&refPuskesmas)
	spew.Dump(err)
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"data":    err,
			"message": err.Error(),
		})
	}

	//start check kd_puskesmas if exist is error
	findKdPuskesmas := db.Debug().Where("kd_puskesmas = ?", refPuskesmas.KdPuskesmas).First(&dataRefPuskesmas)
	if findKdPuskesmas.RowsAffected != 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "kd_puskesmas is already exist",
		})
	}
	//stop check kd_puskesmas if exist is error

	//start check kecamatan_id if not exist is error
	findKecamatanId := db.Debug().Where("kecamatan_id = ?", refPuskesmas.KecamatanId).First(&dataKecamatan)
	if findKecamatanId.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "kecamatan_id is not exist",
		})
	}
	//stop check kecamatan_id if not exist is error

	//start insert data to table ref_puskesmas
	if createRefPuskesmas := db.Debug().Create(&refPuskesmas); createRefPuskesmas.Error != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Failed to insert data to ref_puskesmas table",
		})
	} 
	//stop insert data to table ref_puskesmas

	return c.JSON(http.StatusOK, &map[string]interface{}{
		"error":   false,
		"message": "success",
		"data": refPuskesmas,
		"status": http.StatusOK,

	})
}

// GetRefPuskesmas godoc
// @Summary GET data from the system
// @Tags Input
// @Accept json 
// @Param kd_puskesmas query string false "kd_puskesmas to find related record"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.RefPuskesmas} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/ref-puskesmas [get]
func GetRefPuskesmas(c echo.Context) error {
	db := db.DbManager()
	refPuskesmas := new(model.RefPuskesmas)

	kd_puskesmas := c.QueryParam("kd_puskesmas")

	//get data from ref_puskesmas table
	dataRefPuskesmas := db.Debug().Where("kd_puskesmas = ?", kd_puskesmas).First(&refPuskesmas)
	if dataRefPuskesmas.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
      "message": "Success",
	  "data": dataRefPuskesmas,
	  "error": false,
	  "status": http.StatusOK,

   })
}