package api

import (
	"net/http"
	"iks_golang/db"
	"iks_golang/model"
	"encoding/json"
	// "fmt"
	"github.com/davecgh/go-spew/spew"

	"github.com/labstack/echo/v4"
)


// PostIksInti godoc
// @Summary Input data to the system
// @Tags Input
// @Accept json 
// @Param JSON body model.IksInti true "JSON RAW"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.IksInti} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/iks-inti [post]
func PostIksInti(c echo.Context) (err error) {
	db := db.DbManager()
	iksInti := new(model.IksInti)
	dataIksInti := model.IksInti{} 
	
	//decode json to struct
	err = json.NewDecoder(c.Request().Body).Decode(&iksInti)
	spew.Dump(err)
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"data":    err,
			"message": err.Error(),
		})
	}

	//start check survei_id if exist is error
	findId := db.Debug().Where("survei_id = ?", iksInti.SurveiId).First(&dataIksInti)
	if findId.RowsAffected != 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
            "status": http.StatusBadRequest,
			"message": "survei_id is exist",
		})
	}
	//stop check survei_id if exist is error

	//start insert data to table iks_inti
	if createIksInti := db.Debug().Create(&iksInti); createIksInti.Error != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Failed to insert data to iks_inti table",
		})
	} 
	//stop insert data to table iks_inti

	return c.JSON(http.StatusOK, &map[string]interface{}{
		"error":   false,
		"message": "success",
		"data": iksInti,
		"status": http.StatusOK,

	})
}

// GetIksInti godoc
// @Summary GET data from the system
// @Tags Input
// @Accept json 
// @Param survei_id query string false "survei_id to find related record"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.IksInti} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/iks-inti [get]
func GetIksInti(c echo.Context) error {
	db := db.DbManager()
	iksInti := new(model.IksInti)

	survei_id := c.QueryParam("survei_id")

	//get data from iks_inti table
	dataIksInti := db.Debug().Where("survei_id = ?", survei_id).First(&iksInti)
	if dataIksInti.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
      "message": "Success",
	  "data": dataIksInti,
	  "error": false,
	  "status": http.StatusOK,
   })
}