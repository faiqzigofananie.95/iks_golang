package api

import (
	"net/http"
	"iks_golang/db"
	"iks_golang/model"
	"encoding/json"
	// "fmt"
	"github.com/davecgh/go-spew/spew"

	"github.com/labstack/echo/v4"
)


// PostKecamatan godoc
// @Summary Input data to the system
// @Tags Input
// @Accept json 
// @Param JSON body model.Kecamatan true "JSON RAW"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Kecamatan} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/kecamatan [post]
func PostKecamatan(c echo.Context) (err error) {
	db := db.DbManager()
	kecamatan := new(model.Kecamatan)
	dataKecamatan := model.Kecamatan{} 
	dataKota := model.Kota{} 
	
	//decode json to struct
	err = json.NewDecoder(c.Request().Body).Decode(&kecamatan)
	spew.Dump(err)
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"data":    err,
			"message": err.Error(),
		})
	}

	//start check kecamatan_id if exist is error
	findKecamatanId := db.Debug().Where("kecamatan_id = ?", kecamatan.KecamatanId).First(&dataKecamatan)
	if findKecamatanId.RowsAffected != 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "kecamatan_id is already exist",
		})
	}
	//stop check kecamatan_id if exist is error

	//start check kota_id if not exist is error
	findKotaId := db.Debug().Where("kota_id = ?", kecamatan.KotaId).First(&dataKota)
	if findKotaId.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "kota_id is not exist",
		})
	}
	//stop check kota_id if not exist is error

	//start insert data to table kecamatan
	if createKecamatan := db.Debug().Create(&kecamatan); createKecamatan.Error != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Failed to insert data to kecamatan table",
		})
	} 
	//stop insert data to table kecamatan

	return c.JSON(http.StatusOK, &map[string]interface{}{
		"error":   false,
		"message": "success",
		"data": kecamatan,
		"status": http.StatusOK,

	})
}

// GetKecamatan godoc
// @Summary GET data from the system
// @Tags Input
// @Accept json 
// @Param kecamatan_id query string false "kecamatan_id to find related record"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Kecamatan} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/kecamatan [get]
func GetKecamatan(c echo.Context) error {
	db := db.DbManager()
	kecamatan := new(model.Kecamatan)

	kecamatan_id := c.QueryParam("kecamatan_id")

	//get data from kecamatan table
	dataKecamatan := db.Debug().Where("kecamatan_id = ?", kecamatan_id).First(&kecamatan)
	if dataKecamatan.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
      "message": "Success",
	  "data": dataKecamatan,
	  "error": false,
	  "status": http.StatusOK,

   })
}