package api

import (
	"net/http"
	"iks_golang/db"
	"iks_golang/model"
	"encoding/json"
	// "fmt"
	"github.com/davecgh/go-spew/spew"
	"strings"
	"strconv"
	"math"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/labstack/echo/v4"
)

func Home(c echo.Context) error {

	return c.String(http.StatusOK, "Main hgjg Index")

}


// PostSurvei godoc
// @Summary Input data to the system
// @Tags Input
// @Accept json 
// @Param JSON body model.PostSurvei{survei_rumah_tangga=model.PostSurveiRumahTangga{survei_individu_detail=[]model.PostSurveiIndividuDetail}} true "JSON RAW"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.PostSurvei{survei_rumah_tangga=[]model.PostSurveiRumahTangga{survei_individu_detail=[]model.PostSurveiIndividuDetail}}} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/survei [post]
func PostSurvei(c echo.Context) (err error) {
	db := db.DbManager()
	survei := new(model.PostSurvei)
	dataSurvei := model.PostSurvei{} 
	tutupBuku := model.TutupBuku{}
	provinsi := model.Provinsi{}
	kota := model.Kota{}
	kecamatan := model.Kecamatan{}
	kelurahan := model.Kelurahan{}
	puskesmas := model.RefPuskesmas{}
	agama := []model.Agama{}
	pendidikan := []model.Pendidikan{}
	pekerjaan := []model.Pekerjaan{}
	kode := ""
	jmlUmur1054Thn := 0
	jmlUmur1259Bln := 0
	jmlUmur011Bln := 0
	AllIndividuDetailId := []string {}
	defaultValue := []string{"Y", "T", "N"}
	
	//decode json to struct
	err = json.NewDecoder(c.Request().Body).Decode(&survei)
	spew.Dump(err)
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"data":    err,
			"message": err.Error(),
		})
	}

	//start check survei_id if exist is error
	findId := db.Debug().Where("survei_id = ?", survei.SurveiId).First(&dataSurvei)
	if findId.RowsAffected != 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei_id is exist",
		})
	}
	//stop check survei_id if exist is error

	//start check if survei.Tanggal year status is exist in table tutup_buku
	checkTanggal, err := time.Parse("2006-01-02", survei.Tanggal)
	if err != nil {
       return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.Tanggal format is incorrect",
		})
    } else {
		spew.Dump(checkTanggal)
		tanggal := survei.Tanggal[0:4]
		findTanggal := db.Debug().Where("tahun_selanjutnya = ?", tanggal).Where("status_aktif = ?", 1).First(&tutupBuku)
		if findTanggal.RowsAffected == 0 {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
			"status": http.StatusBadRequest,
				"message": "survei.Tanggal Year is not exist",
			})
		}
	}
	//stop check if survei.Tanggal year status is exist in table tutup_buku

	//start check provinsi, kota, kecamatan, kelurahan, puskesmas is exist 
	findProvinsi := db.Debug().Where("provinsi_id = ?", survei.PropinsiId).Where("provinsi = ?", strings.ToUpper(survei.NmPropinsi)).First(&provinsi)
	if findProvinsi.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.PropinsiId or survei.NmPropinsi is incorrect",
		})
	}
	// spew.Dump(provinsi.ProvinsiId)
	findKota := db.Debug().Where("kota_id = ?", survei.KotaId).Where("kota = ?", strings.ToUpper(survei.NmKota)).Where("provinsi_id = ?", survei.PropinsiId).First(&kota)
	if findKota.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.KotaId or survei.NmKota is incorrect",
		})
	}

	findKecamatan := db.Debug().Where("kecamatan_id = ?", survei.KecamatanId).Where("kecamatan = ?", strings.ToUpper(survei.NmKecamatan)).Where("kota_id = ?", survei.KotaId).First(&kecamatan)
	if findKecamatan.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.KecamatanId or survei.NmKecamatan is incorrect",
		})
	}

	findKelurahan := db.Debug().Where("kelurahan_id = ?", survei.KelurahanId).Where("kelurahan = ?", strings.ToUpper(survei.NmKelurahan)).Where("kecamatan_id = ?", survei.KecamatanId).First(&kelurahan)
	if findKelurahan.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.KelurahanId or survei.NmKelurahan is incorrect",
		})
	}

	findPuskesmas := db.Debug().Where("kd_puskesmas = ?", survei.KdPuskesmas).Where("kecamatan_id = ?", survei.KecamatanId).First(&puskesmas)
	if findPuskesmas.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.KdPuskesmas is incorrect",
		})
	}
	//stop check provinsi, kota, kecamatan, kelurahan, puskesmas is exist 

	//start check iks_besar & iks_inti
	survei.IksBesar = math.Floor(survei.IksBesar*100)/100
	if survei.IksBesar > 1 || survei.IksBesar < 0{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.IksBesar is incorrect",
		})
	}

	survei.IksInti = math.Floor(survei.IksInti*100)/100
	if survei.IksInti > 1 || survei.IksInti < 0{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.IksInti is incorrect",
		})
	}
	//stop check iks_besar & iks_inti

	//start check kode
	kode = strconv.Itoa(survei.PropinsiId) + "." + strconv.Itoa(survei.KotaId) + "." + strconv.Itoa(survei.KecamatanId) + "." + strconv.Itoa(survei.KelurahanId) + "." + survei.RwNew + "." + survei.RtNew + "." + strconv.Itoa(survei.SurveiId)
	spew.Dump(kode) 
	if survei.Kode != kode {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
			"status": http.StatusBadRequest,
				"message": "survei.Kode is incorrect",
			})
	}
	//stop check kode

	//start check created_at & updated_at
	checkCreatedAt, err := time.Parse("2006-01-02", survei.CreatedAt)
	if err != nil {
       return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.CreatedAt format is incorrect",
		})
    } else {
		spew.Dump(checkCreatedAt)
	}

	checkUpdatedAt, err := time.Parse("2006-01-02", survei.UpdatedAt)
	if err != nil {
       return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.UpdatedAt format is incorrect",
		})
    } else {
		spew.Dump(checkUpdatedAt)
	}
	//stop check created_at & updated_at

	//start check status_pindah & status_delete
	if survei.StatusPindah != 1 {
		if survei.StatusPindah != 0{
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "survei.StatusPindah is incorrect",
			})
		}
	}

	// if survei.StatusDelete != 1 {
	// 	if survei.StatusDelete != 0{
	// 		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
	// 			"error":   true, 
	// "status": http.StatusBadRequest,
	// 			"message": "survei.StatusDelete is incorrect",
	// 		})
	// 	}
	// }
	//stop check status_pindah & status_delete

	//start check step_number
	if survei.StepNumber != 5 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.StepNumber is incorrect",
		})
	}
	//stop check step_number

	//start check sumber
	if survei.Sumber != "web" && survei.Sumber != "mobile" && survei.Sumber != "integrasi" {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "survei.Sumber is incorrect",
		})
	}
	//stop check sumber

	//start check survei_id on table survei, survei_rumah_tangga, survei_individu_detail
	surveiRumahTangga := survei.SurveiRumahTangga
	surveiIndividuDetail := surveiRumahTangga.SurveiIndividuDetail

	//start initiate AllIndividuDetailId
	for i, _ := range surveiIndividuDetail{
		AllIndividuDetailId = append(AllIndividuDetailId, strconv.Itoa(surveiIndividuDetail[i].SurveiIndividuDetailId))		
	}
	//stop initiate AllIndividuDetailId

	if survei.SurveiId != surveiRumahTangga.SurveiId {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "invalid survei_id",
		})
	} 

	for i, _ := range surveiIndividuDetail{
		if surveiIndividuDetail[i].SurveiId != surveiRumahTangga.SurveiId{
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "invalid survei_id",
			})
		} 
	}
	//stop check survei_id on table survei, survei_rumah_tangga, survei_individu_detail

	//check surveiRumahTangga.JmlArtWawancara
	if surveiRumahTangga.JmlArtWawancara != len(surveiIndividuDetail){
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.JmlArtWawancara is incorrect",
			})
	}
	//stop surveiRumahTangga.JmlArtWawancara
	
	//check surveiRumahTangga jml_art_10_54_thn, jml_art_12_59_bln, jml_art_0_11_bln	
	for i, _ := range surveiIndividuDetail{
		if surveiIndividuDetail[i].UmurThn >= 10 && surveiIndividuDetail[i].UmurThn <= 54{
			jmlUmur1054Thn = jmlUmur1054Thn + 1
		} else if surveiIndividuDetail[i].UmurBln >= 12 && surveiIndividuDetail[i].UmurBln <= 59{
			jmlUmur1259Bln = jmlUmur1259Bln + 1
		} else if surveiIndividuDetail[i].UmurBln >= 0 &&   surveiIndividuDetail[i].UmurBln <= 11{
			jmlUmur011Bln =  jmlUmur011Bln + 1
		}
	}
	// spew.Dump(jmlUmur1054Thn, jmlUmur1259Bln, jmlUmur011Bln)

	if surveiRumahTangga.JmlArt1054Thn != jmlUmur1054Thn{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.JmlArt1054Thn is incorrect",
		})
	}

	if surveiRumahTangga.JmlArt1259Bln != jmlUmur1259Bln{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.JmlArt1259Bln is incorrect",
		})
	}

	if surveiRumahTangga.JmlArt011Bln != jmlUmur011Bln{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.JmlArt011Bln is incorrect",
		})
	}
	//stop surveiRumahTangga jml_art_10_54_thn, jml_art_12_59_bln, jml_art_0_11_bln	
	
	//start check surveiRumahTangga sab, sat, jk, js, gjb, obat_gjb, pasung, indikator_8, indikator_11, indikator_12
	if surveiRumahTangga.Sab != "Y" && surveiRumahTangga.Sab != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "	",
		})
	}

	if surveiRumahTangga.Sat != "Y" && surveiRumahTangga.Sat != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.Sat is incorrect",
		})
	}

	if surveiRumahTangga.Jk != "Y" && surveiRumahTangga.Jk != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.Jk is incorrect",
		})
	}

	if surveiRumahTangga.Js != "Y" && surveiRumahTangga.Js != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.Js is incorrect",
		})
	}

	if surveiRumahTangga.Gjb != "Y" && surveiRumahTangga.Gjb != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.Gjb is incorrect",
		})
	}

	if surveiRumahTangga.ObatGjb != "Y" && surveiRumahTangga.ObatGjb != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.ObatGjb is incorrect",
		})
	}

	if surveiRumahTangga.Pasung != "Y" && surveiRumahTangga.Pasung != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.Pasung is incorrect",
		})
	}

	if surveiRumahTangga.Indikator8 != "Y" && surveiRumahTangga.Indikator8 != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.Indikator8 is incorrect",
		})
	}

	if surveiRumahTangga.Indikator11 != "Y" && surveiRumahTangga.Indikator11 != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.Indikator11 is incorrect",
		})
	}

	if surveiRumahTangga.Indikator12 != "Y" && surveiRumahTangga.Indikator12 != "T"{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.Indikator12 is incorrect",
		})
	}
	//stop check surveiRumahTangga sab, sat, jk, js, gjb, obat_gjb, pasung, indikator_8, indikator_11, indikator_12

	//start check surveiRumahTangga.created_at & updated_at
	checkRumahTanggaCreatedAt, err := time.Parse("2006-01-02", surveiRumahTangga.CreatedAt)
	if err != nil {
       return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.CreatedAt format is incorrect",
		})
    } else {
		spew.Dump(checkRumahTanggaCreatedAt)
	}

	checkRumahTanggaUpdatedAt, err := time.Parse("2006-01-02", surveiRumahTangga.UpdatedAt)
	if err != nil {
       return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiRumahTangga.UpdatedAt format is incorrect",
		})
    } else {
		spew.Dump(checkRumahTanggaUpdatedAt)
	}
	//stop check surveiRumahTangga.created_at & updated_at

	//start check all condition in surveiIndividuDetail 
	for i, _ := range surveiIndividuDetail{
	//start check surveiIndividuDetail.Tanggal
	checkIndividuDetailTanggal, err := time.Parse("2006-01-02", surveiIndividuDetail[i].Tanggal)
	if err != nil {
       return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Tanggal is incorrect",
		})
    } else {
		spew.Dump(checkIndividuDetailTanggal)
		if surveiIndividuDetail[i].Tanggal != survei.Tanggal{
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Tanggal is incorrect",
			})
		}
	}
	//stop check surveiIndividuDetail.Tanggal

	//start check surveiIndividuDetail jkn, merokok, babj, sab, tb, obat_tb, batuk, hipertensi, obat_hipertensi, tekanan_darah, kb, faskes, asi_eksklusif, imunisasi, pantau_balita, wuh
	if(surveiIndividuDetail[i].Jkn != ""){
		if surveiIndividuDetail[i].Jkn != "Y" && surveiIndividuDetail[i].Jkn != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Jkn is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Merokok != ""){
		if surveiIndividuDetail[i].Merokok != "Y" && surveiIndividuDetail[i].Merokok != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Merokok is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Babj != ""){
		if surveiIndividuDetail[i].Babj != "Y" && surveiIndividuDetail[i].Babj != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Babj is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Sab != ""){
		if surveiIndividuDetail[i].Sab != "Y" && surveiIndividuDetail[i].Sab != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Sab is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Tb != ""){
		if surveiIndividuDetail[i].Tb != "Y" && surveiIndividuDetail[i].Tb != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Tb is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].ObatTb != ""){
		if surveiIndividuDetail[i].ObatTb != "Y" && surveiIndividuDetail[i].ObatTb != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].ObatTb is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Batuk != ""){
		if surveiIndividuDetail[i].Batuk != "Y" && surveiIndividuDetail[i].Batuk != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Batuk is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Hipertensi != ""){
		if surveiIndividuDetail[i].Hipertensi != "Y" && surveiIndividuDetail[i].Hipertensi != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Hipertensi is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].ObatHipertensi != ""){
		if surveiIndividuDetail[i].ObatHipertensi != "Y" && surveiIndividuDetail[i].ObatHipertensi != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].ObatHipertensi is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].TekananDarah != ""){
		if surveiIndividuDetail[i].TekananDarah != "Y" && surveiIndividuDetail[i].TekananDarah != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].TekananDarah is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Kb != ""){
		if surveiIndividuDetail[i].Kb != "Y" && surveiIndividuDetail[i].Kb != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Kb is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Faskes != ""){
		if surveiIndividuDetail[i].Faskes != "Y" && surveiIndividuDetail[i].Faskes != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Faskes is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].AsiEkslusif != ""){
		if surveiIndividuDetail[i].AsiEkslusif != "Y" && surveiIndividuDetail[i].AsiEkslusif != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].AsiEksklusif is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Imunisasi != ""){
		if surveiIndividuDetail[i].Imunisasi != "Y" && surveiIndividuDetail[i].Imunisasi != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Imunisasi is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].PantauBalita != ""){
		if surveiIndividuDetail[i].PantauBalita != "Y" && surveiIndividuDetail[i].PantauBalita != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].PantauBalita is incorrect",
			})
		}
	}

	if(surveiIndividuDetail[i].Wuh != ""){
		if surveiIndividuDetail[i].Wuh != "Y" && surveiIndividuDetail[i].Wuh != "T" {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Wuh is incorrect",
			})
		}
	}
	//stop check surveiIndividuDetail jkn, merokok, babj, sab, tb, obat_tb, batuk, hipertensi, obat_hipertensi, tekanan_darah, kb, faskes, asi_eksklusif, imunisasi, pantau_balita, wuh
	
	//start check surveiIndividuDetail sistolik,diastolik
	surveiIndividuDetail[i].Sistolik = math.Floor(surveiIndividuDetail[i].Sistolik*100)/100
	if surveiIndividuDetail[i].Sistolik <= 0{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Sistolik is incorrect",
		})
	}

	surveiIndividuDetail[i].Diastolik = math.Floor(surveiIndividuDetail[i].Diastolik*100)/100
	if surveiIndividuDetail[i].Diastolik <= 0{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Diastolik is incorrect",
		})
	}
	//stop check surveiIndividuDetail sistolik,diastolik
	
	//start check surveiIndividuDetail.KbFollow
	if surveiIndividuDetail[i].Kb == "Y" {
		if !contains(AllIndividuDetailId, strconv.Itoa(surveiIndividuDetail[i].KbFollow)){
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].KbFollow is incorrect",
			})
		}
	}
	//stop check surveiIndividuDetail.KbFollow

	//start check surveiIndividuDetail.StatusNik
	if surveiIndividuDetail[i].StatusNik != 0 && surveiIndividuDetail[i].StatusNik != 1{
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].StatusNik is incorrect",
		})
	}	
	//stop check surveiIndividuDetail.StatusNik

	//start check surveiIndividuDetail.UmurBln
	if surveiIndividuDetail[i].UmurBln > 127 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].UmurBln is incorrect",
		})
	}
	//stop check surveiIndividuDetail.UmurBln

	//start check surveiIndividuDetail agama_id, pendidikan_id, pekerjaan_id
	findAgama := db.Debug().Where("id = ?", surveiIndividuDetail[i].AgamaId).First(&agama)
	if findAgama.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].AgamaId is incorrect",
		})
	}

	findPendidikan := db.Debug().Where("id = ?", surveiIndividuDetail[i].PendidikanId).First(&pendidikan)
	if findPendidikan.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].PendidikanId is incorrect",
		})
	}

	findPekerjaan := db.Debug().Where("id = ?", surveiIndividuDetail[i].PekerjaanId).First(&pekerjaan)
	if findPekerjaan.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].PekerjaanId is incorrect",
		})
	}
	//stop check surveiIndividuDetail agama_id, pendidikan_id, pekerjaan_id

	//start check surveiIndividuDetail all indikator 1 until 12
		if !contains(defaultValue, surveiIndividuDetail[i].Indikator1){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator1 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator2){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator2 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator3){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator3 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator4){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator4 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator5){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator5 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator6){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator6 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator7){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator7 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator8){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator8 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator9){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator9 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator10){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator10 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator11){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator11 is incorrect",
			})
		} 

		if !contains(defaultValue, surveiIndividuDetail[i].Indikator12){
				return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].Indikator12 is incorrect",
			})
		} 
	//stop check surveiIndividuDetail all indikator 1 until 12

	//start check surveiIndividuDetail.created_at & updated_at
		checkIndividuDetailCreatedAt, err := time.Parse("2006-01-02", surveiIndividuDetail[i].CreatedAt)
		if err != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].CreatedAt format is incorrect",
			})
		} else {
			spew.Dump(checkIndividuDetailCreatedAt)
		}

		checkIndividuDetailUpdatedAt, err := time.Parse("2006-01-02", surveiIndividuDetail[i].UpdatedAt)
		if err != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "surveiIndividuDetail["+strconv.Itoa(i)+"].UpdatedAt format is incorrect",
			})
		} else {
			spew.Dump(checkIndividuDetailUpdatedAt)
		}
	//stop check surveiIndividuDetail.created_at & updated_at
	}
	//stop check all condition in surveiIndividuDetail

	// start insert data to table survei, survei_rumah_tangga, survei_individu_detail
	if createSurvei := db.Debug().Create(&survei); createSurvei.Error != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Failed insert data to survei table",
		})
	} 

	if createSurveiRumahTangga := db.Debug().Create(&surveiRumahTangga); createSurveiRumahTangga.Error != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Failed to insert data to survei_rumah_tangga table",
		})
	}

	for i, _ := range surveiIndividuDetail{
		if createSurveiIndividuDetail := db.Debug().Create(&surveiIndividuDetail[i]); createSurveiIndividuDetail.Error != nil {
			return c.JSON(http.StatusBadRequest, &map[string]interface{}{
				"error":   true, 
				"status": http.StatusBadRequest,
				"message": "Failed to insert data to survei_individu_detail table",
			})
		}
	}
	// stop insert data to table survei, survei_rumah_tangga, survei_individu_detail

	return c.JSON(http.StatusOK, &map[string]interface{}{
		"error":   false,
		"message": "success",
		"data": survei,
	})
}

// GetSurvei godoc
// @Summary GET data from the system
// @Tags Input
// @Accept json 
// @Param survei_id query string false "survei_id to find related record"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.PostSurvei{survei_rumah_tangga=[]model.PostSurveiRumahTangga{survei_individu_detail=[]model.PostSurveiIndividuDetail}}} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/survei [get]
func GetSurvei(c echo.Context) error {
	// auth:= Auth()
	// if err != nil {
	// 	return c.JSON(http.StatusBadRequest, &map[string]interface{}{
	// 		"error":   true, 
	// 		"status": http.StatusBadRequest,
	// 		"message": "Error generating token string",
	// 	})
	// }

	db := db.DbManager()
	survei := new(model.PostSurvei)

	survei_id := c.QueryParam("survei_id")

	//get data from survei table
	dataSurvei := db.Debug().Where("survei_id = ?", survei_id).First(&survei)
	if dataSurvei.RowsAffected == 0 {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("Record is not exist")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	//get data from survei_rumah_tangga table
	dataSurveiRumahTangga := db.Debug().Where("survei_id = ?", survei_id).First(&survei.SurveiRumahTangga)
	if dataSurveiRumahTangga.RowsAffected == 0 {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("Record is not exist")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	//get data from survei_individu_detail table
	dataSurveiIndividuDetail := db.Debug().Where("survei_id = ?", survei_id).Find(&survei.SurveiRumahTangga.SurveiIndividuDetail)
	if dataSurveiIndividuDetail.RowsAffected == 0 {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("Record is not exist")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status":  http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
      "message": "Success",
	  "data": dataSurvei,
	  "error": false,
	  "status": http.StatusOK,

   })
}

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}