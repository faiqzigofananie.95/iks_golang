package api

import (
	"net/http"
	"iks_golang/db"
	"iks_golang/model"
	"encoding/json"
	// "fmt"
	"github.com/davecgh/go-spew/spew"

	"github.com/labstack/echo/v4"
)


// PostKelurahan godoc
// @Summary Input data to the system
// @Tags Input
// @Accept json 
// @Param JSON body model.Kelurahan true "JSON RAW"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Kelurahan} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/kelurahan [post]
func PostKelurahan(c echo.Context) (err error) {
	db := db.DbManager()
	kelurahan := new(model.Kelurahan)
	dataKelurahan := model.Kelurahan{} 
	dataKecamatan := model.Kecamatan{} 
	
	//decode json to struct
	err = json.NewDecoder(c.Request().Body).Decode(&kelurahan)
	spew.Dump(err)
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"data":    err,
			"message": err.Error(),
		})
	}

	//start check kelurahan_id if exist is error
	findKelurahanId := db.Debug().Where("kelurahan_id = ?", kelurahan.KelurahanId).First(&dataKelurahan)
	if findKelurahanId.RowsAffected != 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "kelurahan_id is already exist",
		})
	}
	//stop check kelurahan_id if exist is error

	//start check kecamatan_id if not exist is error
	findKecamatanId := db.Debug().Where("kecamatan_id = ?", kelurahan.KecamatanId).First(&dataKecamatan)
	if findKecamatanId.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "kecamatan_id is not exist",
		})
	}
	//stop check kecamatan_id if not exist is error

	//start insert data to table kelurahan
	if createKelurahan := db.Debug().Create(&kelurahan); createKelurahan.Error != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Failed to insert data to kelurahan table",
		})
	} 
	//stop insert data to table kelurahan

	return c.JSON(http.StatusOK, &map[string]interface{}{
		"error":   false,
		"message": "success",
		"data": kelurahan,
		"status": http.StatusOK,

	})
}

// GetKelurahan godoc
// @Summary GET data from the system
// @Tags Input
// @Accept json 
// @Param kelurahan_id query string false "kelurahan_id to find related record"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Kelurahan} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/kelurahan [get]
func GetKelurahan(c echo.Context) error {
	db := db.DbManager()
	kelurahan := new(model.Kelurahan)

	kelurahan_id := c.QueryParam("kelurahan_id")

	//get data from kelurahan table
	dataKelurahan := db.Debug().Where("kelurahan_id = ?", kelurahan_id).First(&kelurahan)
	if dataKelurahan.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
      "message": "Success",
	  "data": dataKelurahan,
	  "error": false,
	  "status": http.StatusOK,
   })
}