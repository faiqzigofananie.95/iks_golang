package api

import (
	"iks_golang/config"
	"iks_golang/db"
	"iks_golang/model"
	"net/http"
	// "github.com/davecgh/go-spew/spew"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

// Login godoc
// @Summary Logs user into the system
// @Tags Authentication
// @Accept */*
// @Param username query string true "The user name for login"
// @Param password query string true "The password for login in clear text"
// @Produce application/json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Data} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /login [post]
func Login(c echo.Context) error {
	db := db.DbManager()
	user := model.User{}
	username := c.FormValue("username")
	password := c.FormValue("password")

	if username == "" || password == ""  {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("Must fill username and password")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Must fill username and password",
		})
	}

	dataUser := db.Debug().Where("username = ?", username).Where("status = ?", 1).Where("deleted_at IS NULL").First(&user)
	if dataUser.RowsAffected == 0 {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("Record is not exist")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	match := CheckPasswordHash(password, user.Password)
	if match == false{
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("wrong password")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "wrong password",
		})
	}

	tokenString, err := GenerateJWT(username, user.KdProvinsi)
	if err != nil {
		fmt.Println("Error generating token string")
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"message":     "success",
		"username":    user.Username,
		"kd_provinsi": user.KdProvinsi,
		"token":       tokenString,
		"error":       "false",
	})
}

// Register godoc
// @Summary Regists user into the system
// @Tags Authentication
// @Accept */*
// @Param username query string true "The user name for login"
// @Param password query string true "The password for login in clear text"
// @Produce application/json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Data} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /register [post]
func Register(c echo.Context) error {
	db := db.DbManager()
	user := model.User{}
	name := c.FormValue("name")
	username := c.FormValue("username")
	email := c.FormValue("email")
	password := c.FormValue("password")
	kdPuskesmas := c.FormValue("kd_puskesmas")
	supervisor := c.FormValue("supervisor")
	kdProvinsi := c.FormValue("kd_provinsi")
	nik := c.FormValue("nik")

	if name == "" || email == "" || password == "" || kdProvinsi == "" {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), }).Error("name and email cant be blank")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "name and email cant be blank",
		})
	}

	dataUser := db.Debug().Where("kd_provinsi = ?", kdProvinsi).Where("status = ?", 1).Where("deleted_at IS NULL").First(&user)
	if dataUser.RowsAffected != 0 {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("Province is already used")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Province is already used",
		})
	}

	hashPassword, _ := HashPassword(password)

	tokenString, err := GenerateJWT(username, kdProvinsi)
	if err != nil {
		fmt.Println("Error generating token string")
	}

	newUser := model.User{
		Name:			name,
		Username:		username,							
		Email:			email,
		Password:		hashPassword,
		KdPuskesmas:	kdPuskesmas,
		KdProvinsi:		kdProvinsi,
		Supervisor:		supervisor,
		Type:			"iks",
		Nik: 			nik,
		TokenNik:		tokenString,
		Status:			1,
	} 
	db.Debug().Create(&newUser)

	return c.JSON(http.StatusOK, map[string]interface{}{
		"message":     "success",
		"token":       tokenString,
		"error":       "false",
	})
}

// Update User godoc
// @Summary Update user into the system
// @Tags Authentication
// @Accept */*
// @Param username query string true "The user name for login"
// @Param password query string true "The password for login in clear text"
// @Produce application/json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Data} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /update-user [post]
func UpdateUser(c echo.Context) error {
	db := db.DbManager()
	id := c.FormValue("id")
	name := c.FormValue("name")
	username := c.FormValue("username")
	email := c.FormValue("email")
	password := c.FormValue("password")
	kdPuskesmas := c.FormValue("kd_puskesmas")
	supervisor := c.FormValue("supervisor")
	kdProvinsi := c.FormValue("kd_provinsi")
	nik := c.FormValue("nik")

	if id == "" || name == "" || email == "" || password == "" || kdProvinsi == "" {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("field id, name, email, password, kdProvinsi cant be blank")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "field id, name, email, password, kdProvinsi cant be blank",
		})
	}

	hashPassword, _ := HashPassword(password)

	User := model.User{}
	User.Name = name
	User.Username = username
	User.Email = email
	User.Password = hashPassword
	User.KdPuskesmas = kdPuskesmas 
	User.Nik = nik
	User.Supervisor = supervisor

	userUpdate := db.Debug().Model(User).Where("id=?", id).Where("deleted_at IS NULL").Updates(&User)
	if userUpdate.RowsAffected == 0 {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("Update Failed")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Update failed",
		})
	} 
	
	return c.JSON(http.StatusOK, map[string]interface{}{
		"message":     "Update Success",
		"error":       "false",
	})
	
}

// Delete User godoc
// @Summary Delete user into the system
// @Tags Authentication
// @Accept */*
// @Param username query string true "The user name for login"
// @Param password query string true "The password for login in clear text"
// @Produce application/json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Data} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /delete-user [post]
func DeleteUser(c echo.Context) error {
	db := db.DbManager()
	id := c.QueryParam("id")

	if id == "" {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("field id cant be blank")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
"status": http.StatusBadRequest,
			"message": "field id cant be blank",
		})
	}



	User := model.User{}

	userDelete := db.Debug().Model(User).Where("id=?", id).Where("deleted_at IS NULL").Delete(&User)
	if userDelete.RowsAffected == 0 {
		log.WithFields(log.Fields{"method": c.Request().Method, "path": c.Path(), "status": 400, "latency_ns": time.Since(time.Now()).Nanoseconds(), 
		}).Error("field id cant be blank")
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Delete failed",
		})
	} 
	
	return c.JSON(http.StatusOK, map[string]interface{}{
		"message":     "Delete Success",
		"error":       "false",
	})
	
}



func HashPassword(password string) (string, error){
	passwordBcrypt, err := bcrypt.GenerateFromPassword([]byte(password), 10) 
	return string(passwordBcrypt), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func GenerateJWT(user, kdProvinsi string) (string, error) {
	configuration := config.GetConfig()
	mySigningKey := []byte(configuration.SECRET_KEY)
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = user
	claims["kd_provinsi"] = kdProvinsi
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()

	// Create the JWT string.
	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		fmt.Errorf("Something went wrong: %s", err.Error())
		return "", err
	}

	return tokenString, nil
}

func Auth(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(model.Claims)
	// name := claims.Name

	return c.JSON(http.StatusOK, map[string]interface{}{
		"message":  "success",
		"username": claims,
		//   "kd_provinsi": user.KdProvinsi,
		"error": "false",
	})
}
