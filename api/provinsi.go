package api

import (
	"net/http"
	"iks_golang/db"
	"iks_golang/model"
	"encoding/json"
	// "fmt"
	"github.com/davecgh/go-spew/spew"

	"github.com/labstack/echo/v4"
)


// PostProvinsi godoc
// @Summary Input data to the system
// @Tags Input
// @Accept json 
// @Param JSON body model.Provinsi true "JSON RAW"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Provinsi} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/provinsi [post]
func PostProvinsi(c echo.Context) (err error) {
	db := db.DbManager()
	provinsi := new(model.Provinsi)
	dataProvinsi := model.Provinsi{} 
	
	//decode json to struct
	err = json.NewDecoder(c.Request().Body).Decode(&provinsi)
	spew.Dump(err)
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"data":    err,
			"message": err.Error(),
		})
	}

	//start check provinsi_id if exist is error
	findId := db.Debug().Where("provinsi_id = ?", provinsi.ProvinsiId).First(&dataProvinsi)
	if findId.RowsAffected != 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "provinsi_id is exist",
		})
	}
	//stop check provinsi_id if exist is error

	//start insert data to table provinsi
	if createProvinsi := db.Debug().Create(&provinsi); createProvinsi.Error != nil {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Failed to insert data to provinsi table",
		})
	} 
	//stop insert data to table provinsi

	return c.JSON(http.StatusOK, &map[string]interface{}{
		"error":   false,
		"message": "success",
		"data": provinsi,
		"status": http.StatusOK,

	})
}

// GetProvinsi godoc
// @Summary GET data from the system
// @Tags Input
// @Accept json 
// @Param provinsi_id query string false "provinsi_id to find related record"
// @produce json
// @success 200 {object} model.JSONResultSuccess{data=[]model.Provinsi} "success"
// @failure 400	{object} model.JSONResultError{} "Invalid username/password supplied"
// @Router /api/provinsi [get]
func GetProvinsi(c echo.Context) error {
	db := db.DbManager()
	provinsi := new(model.Provinsi)

	provinsi_id := c.QueryParam("provinsi_id")

	//get data from iks_inti table
	dataProvinsi := db.Debug().Where("provinsi_id = ?", provinsi_id).First(&provinsi)
	if dataProvinsi.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, &map[string]interface{}{
			"error":   true, 
			"status": http.StatusBadRequest,
			"message": "Record is not exist",
		})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
      "message": "Success",
	  "data": dataProvinsi,
	  "error": false,
	  "status": http.StatusOK,

   })
}