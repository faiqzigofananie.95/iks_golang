module iks_golang

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.6.1
	github.com/sirupsen/logrus v1.8.1
	github.com/swaggo/echo-swagger v1.1.4
	github.com/swaggo/swag v1.7.4
	github.com/tkanos/gonfig v0.0.0-20210106201359-53e13348de2f
	golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871
)
