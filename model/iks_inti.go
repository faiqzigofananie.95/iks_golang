package model

import (
	// "time"
)

type IksInti struct {
	IksIntiId              int    	 `gorm:"column:iks_inti_id" json:"iks_inti_id" form:"iks_inti_id" `
	SurveiId               int    	 `gorm:"column:survei_id" json:"survei_id" form:"survei_id" `
	Indikator1             string    `gorm:"column:indikator_1" json:"indikator_1" form:"indikator_1"`
	Indikator2             string    `gorm:"column:indikator_2" json:"indikator_2" form:"indikator_2"`
	Indikator3             string    `gorm:"column:indikator_3" json:"indikator_3" form:"indikator_3"`
	Indikator4             string    `gorm:"column:indikator_4" json:"indikator_4" form:"indikator_4"`
	Indikator5             string    `gorm:"column:indikator_5" json:"indikator_5" form:"indikator_5"`
	Indikator6             string    `gorm:"column:indikator_6" json:"indikator_6" form:"indikator_6"`
	Indikator7             string    `gorm:"column:indikator_7" json:"indikator_7" form:"indikator_7"`
	Indikator8             string    `gorm:"column:indikator_8" json:"indikator_8" form:"indikator_8"`
	Indikator9             string    `gorm:"column:indikator_9" json:"indikator_9" form:"indikator_9"`
	Indikator10            string    `gorm:"column:indikator_10" json:"indikator_10" form:"indikator_10"`
	Indikator11            string    `gorm:"column:indikator_11" json:"indikator_11" form:"indikator_11"`
	Indikator12            string    `gorm:"column:indikator_12" json:"indikator_12" form:"indikator_12"`
	Status 				   int		 `gorm:"column:status" json:"status" form:"status"`	
	StatusData 			   int		 `gorm:"column:status_data" json:"status_data" form:"status_data"`	
	Kode                   string    `gorm:"column:kode" json:"kode" form:"kode"`
	IksInti 			   float32   `gorm:"column:iks_inti" json:"iks_inti" form:"iks_inti"`
	KdPuskesmas 		   string	 `gorm:"column:kd_puskesmas" json:"kd_puskesmas" form:"kd_puskesmas"`
	NoUrutRt               string    `gorm:"column:no_urut_rt" json:"no_urut_rt" form:"no_urut_rt"`
	NoUrutKel              string    `gorm:"column:no_urut_kel" json:"no_urut_kel" form:"no_urut_kel"`
	Supervisor             int       `gorm:"column:supervisor" json:"supervisor" form:"supervisor"`
	Surveyor               int       `gorm:"column:surveyor" json:"surveyor" form:"surveyor"`
	StatusDelete           int       `gorm:"column:status_delete" json:"status_delete" form:"status_delete"`
	CreatedAt              string 	 `gorm:"column:created_at" json:"created_at" form:"created_at"`
	UpdatedAt              string 	 `gorm:"column:updated_at" json:"updated_at" form:"updated_at"`
}

func (IksInti) TableName() string {
	return "iks_inti"
}