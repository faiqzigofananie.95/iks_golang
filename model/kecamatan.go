package model

import (
	// "time"
)

type Kecamatan struct {
	KecamatanId            int    	 `gorm:"column:kecamatan_id" json:"kecamatan_id" form:"kecamatan_id" `
	Kecamatan              string    `gorm:"column:kecamatan" json:"kecamatan" form:"kecamatan" `
	KotaId             	   int    	 `gorm:"column:kota_id" json:"kota_id" form:"kota_id" `	
}

func (Kecamatan) TableName() string {
	return "kecamatan"
}