package model

import (
	// "time"
)

type Pekerjaan struct {
	Id       			 int    `gorm:"column:id" json:"id" form:"id" `
	Pekerjaan     	     string  `gorm:"column:pekerjaan" json:"pekerjaan" form:"pekerjaan" `
	CreatedAt            string `gorm:"column:created_at" json:"created_at" form:"created_at" `
	UpdatedAt            string `gorm:"column:updated_at" json:"updated_at" form:"updated_at" `

}

func (Pekerjaan) TableName() string {
	return "ref_pekerjaan"
}
