package model

import (
	// "time"
)

type Kota struct {
	KotaId             	   int    	 `gorm:"column:kota_id" json:"kota_id" form:"kota_id" `
	Kota                   string    `gorm:"column:kota" json:"kota" form:"kota" `
	ProvinsiId             int 	 	 `gorm:"column:provinsi_id" json:"provinsi_id" form:"provinsi_id"`	
}

func (Kota) TableName() string {
	return "kota"
}