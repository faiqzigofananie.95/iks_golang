package model

import (
	// "time"
)

type RefPuskesmas struct {
	KdPuskesmas            string    `gorm:"column:kd_puskesmas" json:"kd_puskesmas" form:"kd_puskesmas" `
	KecamatanId            int    	 `gorm:"column:kecamatan_id" json:"kecamatan_id" form:"kecamatan_id" `
	Puskesmas              string    `gorm:"column:puskesmas" json:"puskesmas" form:"puskesmas" `
	Alamat		           string    `gorm:"column:alamat" json:"alamat" form:"alamat" `
	JenisPuskesmasId       int	     `gorm:"column:jenis_puskesmas_id" json:"jenis_puskesmas_id" form:"jenis_puskesmas_id" `
	KdPusBps	           string    `gorm:"column:kd_pus_bps" json:"kd_pus_bps" form:"kd_pus_bps" `	
	WilayahKerja           string    `gorm:"column:wilayah_kerja" json:"wilayah_kerja" form:"wilayah_kerja" `
	KecIdOpt               string    `gorm:"column:kec_id_opt" json:"kec_id_opt" form:"kec_id_opt" `
	WilayahKerjaOpt        string    `gorm:"column:wilayah_kerja_opt" json:"wilayah_kerja_opt" form:"wilayah_kerja_opt" `		
}

func (RefPuskesmas) TableName() string {
	return "ref_puskesmas"
}