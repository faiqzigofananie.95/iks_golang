package model

import (
	jwt "github.com/dgrijalva/jwt-go"
)

type User struct {
	Id               int    `gorm:"column:id" json:"id" form:"id" `
	Name             string    `gorm:"column:name" json:"name" form:"name" `
	Email            string `gorm:"column:email" json:"email" form:"email" `
	Username         string `gorm:"column:username" json:"username" form:"username"`
	Password         string `gorm:"column:password" json:"password" form:"password"`
	Status           int    `gorm:"column:status" json:"status" form:"status"`
	// ConfirmationCode string `gorm:"column:confirmation_code" json:"confirmation_code" form:"confirmation_code"`
	Confirmed        int    `gorm:"column:confirmed" json:"confirmed" form:"confirmed"`
	KdPuskesmas      string `gorm:"column:kd_puskesmas" json:"kd_puskesmas" form:"kd_puskesmas"`
	Supervisor       string `gorm:"column:supervisor" json:"supervisor" form:"supervisor"`
	TokenNik         string `gorm:"column:token_nik" json:"token_nik" form:"token_nik"`
	KdProvinsi       string `gorm:"column:kd_provinsi" json:"kd_provinsi" form:"kd_provinsi"`
	Type             string `gorm:"column:type" json:"type" form:"type"`
	CreatedBy        int    `gorm:"column:created_by" json:"created_by" form:"created_by"`
	Nik              string `gorm:"column:nik" json:"nik" form:"nik"`
}

type Claims struct {
	Name       string `json:"name"`
	KdProvinsi string `json:"kd_provinsi"`
	jwt.StandardClaims
}
