package model

import (
	// "time"
)

type PostSurvei struct {
	SurveiId             int    `gorm:"column:survei_id" json:"survei_id" form:"survei_id" `
	Tanggal              string `gorm:"column:tanggal" json:"tanggal" form:"tanggal" `
	NoUrutRt             string    `gorm:"column:no_urut_rt" json:"no_urut_rt" form:"no_urut_rt" `
	NoUrutKel            string    `gorm:"column:no_urut_kel" json:"no_urut_kel" form:"no_urut_kel" `
	Surveyor             int    `gorm:"column:surveyor" json:"surveyor" form:"surveyor" `
	Supervisor           int    	`gorm:"column:supervisor" json:"supervisor" form:"supervisor" `
	NoKk                 string    `gorm:"column:no_kk" json:"no_kk" form:"no_kk" `
	NamaKk               string    `gorm:"column:nama_kk" json:"nama_kk" form:"nama_kk" `
	Alamat               string    `gorm:"column:alamat" json:"alamat" form:"alamat" `
	PropinsiId           int    `gorm:"column:propinsi_id" json:"propinsi_id" form:"propinsi_id" `
	KotaId               int    `gorm:"column:kota_id" json:"kota_id" form:"kota_id" `
	KecamatanId          int    `gorm:"column:kecamatan_id" json:"kecamatan_id" form:"kecamatan_id" `
	KelurahanId          int    `gorm:"column:kelurahan_id" json:"kelurahan_id" form:"kelurahan_id" `
	KdPuskesmas          string    `gorm:"column:kd_puskesmas" json:"kd_puskesmas" form:"kd_puskesmas" `
	NmKota               string    `gorm:"column:nm_kota" json:"nm_kota" form:"nm_kota" `
	NmPropinsi           string    `gorm:"column:nm_propinsi" json:"nm_propinsi" form:"nm_propinsi" `
	NmKecamatan          string    `gorm:"column:nm_kecamatan" json:"nm_kecamatan" form:"nm_kecamatan" `
	NmKelurahan          string    `gorm:"column:nm_kelurahan" json:"nm_kelurahan" form:"nm_kelurahan" `
	IksBesar             float64   `gorm:"column:iks_besar" json:"iks_besar" form:"iks_besar" `
	IksInti              float64   `gorm:"column:iks_inti" json:"iks_inti" form:"iks_inti" `
	RtNew                string    `gorm:"column:rt_new" json:"rt_new" form:"rt_new" `
	RwNew                string    `gorm:"column:rw_new" json:"rw_new" form:"rw_new" `
	Kode                 string    `gorm:"column:kode" json:"kode" form:"kode" `
	CreatedAt            string `gorm:"column:created_at" json:"created_at" form:"created_at" `
	UpdatedAt            string `gorm:"column:updated_at" json:"updated_at" form:"updated_at" `
	StatusPindah         int       `gorm:"column:status_pindah" json:"status_pindah" form:"status_pindah" `
	StatusDelete         int       `gorm:"column:status_delete" json:"status_delete" form:"status_delete" `
	StepNumber           int       `gorm:"column:step_number" json:"step_number" form:"step_number" `
	Sumber               string    `gorm:"column:sumber" json:"sumber" form:"sumber" `
	StatusKepalaKeluarga int       `gorm:"column:status_kepala_keluarga" json:"status_kepala_keluarga" form:"status_kepala_keluarga" `
	RtOld                string    `gorm:"column:rt_old" json:"rt_old" form:"rt_old" `
	RwOld                string    `gorm:"column:rw_old" json:"rw_old" form:"rw_old" `
	StatusMigrasi        int       `gorm:"column:status_migrasi" json:"status_migrasi" form:"status_migrasi" `
	CatatanMigrasi       string    `gorm:"column:catatan_migrasi" json:"catatan_migrasi" form:"catatan_migrasi" `
	IksIntiOld           float32   `gorm:"column:iks_inti_old" json:"iks_inti_old" form:"iks_inti_old" `
	IksBesarOld          float32   `gorm:"column:iks_besar_old" json:"iks_besar_old" form:"iks_besar_old" `
	// DeletedAt            string `gorm:"column:deleted_at" json:"deleted_at" form:"deleted_at" `
	SurveiRumahTangga 	 PostSurveiRumahTangga  	`json:"survei_rumah_tangga"`
}

func (PostSurvei) TableName() string {
	return "survei"
}

type PostSurveiIndividuDetail struct {
	SurveiIndividuDetailId int    `gorm:"column:survei_individu_detail_id" json:"survei_individu_detail_id" form:"survei_individu_detail_id"`
	Tanggal                string `gorm:"column:tanggal" json:"tanggal" form:"tanggal"`
	Jkn                    string    `gorm:"column:jkn" json:"jkn" form:"jkn"`
	Merokok                string    `gorm:"column:merokok" json:"merokok" form:"merokok"`
	Babj                   string    `gorm:"column:babj" json:"babj" form:"babj"`
	Sab                    string    `gorm:"column:sab" json:"sab" form:"sab"`
	Tb                     string    `gorm:"column:tb" json:"tb" form:"tb"`
	ObatTb                 string    `gorm:"column:obat_tb" json:"obat_tb" form:"obat_tb"`
	Batuk                  string    `gorm:"column:batuk" json:"batuk" form:"batuk"`
	Hipertensi             string    `gorm:"column:hipertensi" json:"hipertensi" form:"hipertensi"`
	ObatHipertensi         string    `gorm:"column:obat_hipertensi" json:"obat_hipertensi" form:"obat_hipertensi"`
	TekananDarah           string    `gorm:"column:tekanan_darah" json:"tekanan_darah" form:"tekanan_darah"`
	Sistolik               float64   `gorm:"column:sistolik" json:"sistolik" form:"sistolik"`
	Diastolik              float64   `gorm:"column:diastolik" json:"diastolik" form:"diastolik"`
	Kb                     string    `gorm:"column:kb" json:"kb" form:"kb"`
	KbFollow               int    	 `gorm:"column:kb_follow" json:"kb_follow" form:"kb_follow"`
	KbDetail               int       `gorm:"column:kb_detail" json:"kb_detail" form:"kb_detail"`
	Faskes                 string    `gorm:"column:faskes" json:"faskes" form:"faskes"`
	AsiEkslusif            string    `gorm:"column:asi_ekslusif" json:"asi_ekslusif" form:"asi_ekslusif"`
	Imunisasi              string    `gorm:"column:imunisasi" json:"imunisasi" form:"imunisasi"`
	PantauBalita           string    `gorm:"column:pantau_balita" json:"pantau_balita" form:"pantau_balita"`
	Keterangan             string    `gorm:"column:keterangan" json:"keterangan" form:"keterangan"`
	CatatanIntervensi      string    `gorm:"column:catatan_intervensi" json:"catatan_intervensi" form:"catatan_intervensi"`
	NoUrutRt               string    `gorm:"column:no_urut_rt" json:"no_urut_rt" form:"no_urut_rt"`
	NoUrutKel              string    `gorm:"column:no_urut_kel" json:"no_urut_kel" form:"no_urut_kel"`
	Nik                    string    `gorm:"column:nik" json:"nik" form:"nik"`
	StatusNik              int       `gorm:"column:status_nik" json:"status_nik" form:"status_nik"`
	Nama                   string    `gorm:"column:nama" json:"nama" form:"nama"`
	HubKelId               int       `gorm:"column:hub_kel_id" json:"hub_kel_id" form:"hub_kel_id"`
	TanggalLahir           string `gorm:"column:tanggal_lahir" json:"tanggal_lahir" form:"tanggal_lahir"`
	UmurBln                int       `gorm:"column:umur_bln" json:"umur_bln" form:"umur_bln"`
	UmurThn                int       `gorm:"column:umur_thn" json:"umur_thn" form:"umur_thn"`
	JkId                   int       `gorm:"column:jk_id" json:"jk_id" form:"jk_id"`
	StmaritalId            int       `gorm:"column:stmarital_id" json:"stmarital_id" form:"stmarital_id"`
	Wuh                    string    `gorm:"column:wuh" json:"wuh" form:"wuh"`
	AgamaId                int       `gorm:"column:agama_id" json:"agama_id" form:"agama_id"`
	PendidikanId           int       `gorm:"column:pendidikan_id" json:"pendidikan_id" form:"pendidikan_id"`
	PekerjaanId            int       `gorm:"column:pekerjaan_id" json:"pekerjaan_id" form:"pekerjaan_id"`
	Indikator1             string    `gorm:"column:indikator_1" json:"indikator_1" form:"indikator_1"`
	Indikator2             string    `gorm:"column:indikator_2" json:"indikator_2" form:"indikator_2"`
	Indikator3             string    `gorm:"column:indikator_3" json:"indikator_3" form:"indikator_3"`
	Indikator4             string    `gorm:"column:indikator_4" json:"indikator_4" form:"indikator_4"`
	Indikator5             string    `gorm:"column:indikator_5" json:"indikator_5" form:"indikator_5"`
	Indikator6             string    `gorm:"column:indikator_6" json:"indikator_6" form:"indikator_6"`
	Indikator7             string    `gorm:"column:indikator_7" json:"indikator_7" form:"indikator_7"`
	Indikator8             string    `gorm:"column:indikator_8" json:"indikator_8" form:"indikator_8"`
	Indikator9             string    `gorm:"column:indikator_9" json:"indikator_9" form:"indikator_9"`
	Indikator10            string    `gorm:"column:indikator_10" json:"indikator_10" form:"indikator_10"`
	Indikator11            string    `gorm:"column:indikator_11" json:"indikator_11" form:"indikator_11"`
	Indikator12            string    `gorm:"column:indikator_12" json:"indikator_12" form:"indikator_12"`
	Kode                   string    `gorm:"column:kode" json:"kode" form:"kode"`
	Supervisor             int       `gorm:"column:supervisor" json:"supervisor" form:"supervisor"`
	Surveyor               int       `gorm:"column:surveyor" json:"surveyor" form:"surveyor"`
	StatusDelete           int       `gorm:"column:status_delete" json:"status_delete" form:"status_delete"`
	CreatedAt              string `gorm:"column:created_at" json:"created_at" form:"created_at"`
	UpdatedAt              string `gorm:"column:updated_at" json:"updated_at" form:"updated_at"`
	SurveiId               int    `gorm:"column:survei_id" json:"survei_id" form:"survei_id"`
	StatusSurvei           int       `gorm:"column:status_survei" json:"status_survei" form:"status_survei"`
	// DeletedAt              string `gorm:"column:deleted_at" json:"deleted_at" form:"deleted_at"`
	KdPuskesmas            string    `gorm:"column:kd_puskesmas" json:"kd_puskesmas" form:"kd_puskesmas"`
}

func (PostSurveiIndividuDetail) TableName() string {
	return "survei_individu_detail"
}

type PostSurveiRumahTangga struct {
	SurveiRtId      int    `gorm:"column:survei_rt_id" json:"survei_rt_id" form:"survei_rt_id"`
	SurveiId        int    `gorm:"column:survei_id" json:"survei_id" form:"survei_id"`
	JmlArt          int       `gorm:"column:jml_art" json:"jml_art" form:"jml_art"`
	JmlArtWawancara int       `gorm:"column:jml_art_wawancara" json:"jml_art_wawancara" form:"jml_art_wawancara"`
	JmlArtDewasa    int       `gorm:"column:jml_art_dewasa" json:"jml_art_dewasa" form:"jml_art_dewasa"`
	JmlArt1054Thn   int       `gorm:"column:jml_art_10_54_thn" json:"jml_art_10_54_thn" form:"jml_art_10_54_thn"`
	JmlArt1259Bln   int       `gorm:"column:jml_art_12_59_bln" json:"jml_art_12_59_bln" form:"jml_art_12_59_bln"`
	JmlArt011Bln    int       `gorm:"column:jml_art_0_11_bln" json:"jml_art_0_11_bln" form:"jml_art_0_11_bln"`
	Sab             string    `gorm:"column:sab" json:"sab" form:"sab"`
	Sat             string    `gorm:"column:sat" json:"sat" form:"sat"`
	Jk              string    `gorm:"column:jk" json:"jk" form:"jk"`
	Js              string    `gorm:"column:js" json:"js" form:"js"`
	Gjb             string    `gorm:"column:gjb" json:"gjb" form:"gjb"`
	ObatGjb         string    `gorm:"column:obat_gjb" json:"obat_gjb" form:"obat_gjb"`
	Pasung          string    `gorm:"column:pasung" json:"pasung" form:"pasung"`
	Keterangan      string    `gorm:"column:keterangan" json:"keterangan" form:"keterangan"`
	Kode            string    `gorm:"column:kode" json:"kode" form:"kode"`
	Supervisor      int       `gorm:"column:supervisor" json:"supervisor" form:"supervisor"`
	Surveyor        int       `gorm:"column:surveyor" json:"surveyor" form:"surveyor"`
	Indikator8      string    `gorm:"column:indikator_8" json:"indikator_8" form:"indikator_8"`
	Indikator11     string    `gorm:"column:indikator_11" json:"indikator_11" form:"indikator_11"`
	Indikator12     string    `gorm:"column:indikator_12" json:"indikator_12" form:"indikator_12"`
	StatusDelete    int       `gorm:"column:status_delete" json:"status_delete" form:"status_delete"`
	CreatedAt       string `gorm:"column:created_at" json:"created_at" form:"created_at"`
	UpdatedAt       string `gorm:"column:updated_at" json:"updated_at" form:"updated_at"`
	// DeletedAt       string `gorm:"column:deleted_at" json:"deleted_at" form:"deleted_at"`
	KdPuskesmas     string    `gorm:"column:kd_puskesmas" json:"kd_puskesmas" form:"kd_puskesmas"`
	SurveiIndividuDetail 	 []PostSurveiIndividuDetail  	`json:"survei_individu_detail"`
}

func (PostSurveiRumahTangga) TableName() string {
	return "survei_rumah_tangga"
}





