package model

import (
	// "time"
)

type Pendidikan struct {
	Id       			 int    `gorm:"column:id" json:"id" form:"id" `
	Pendidikan     	     string  `gorm:"column:pendidikan" json:"pendidikan" form:"pendidikan" `
	CreatedAt            string `gorm:"column:created_at" json:"created_at" form:"created_at" `
	UpdatedAt            string `gorm:"column:updated_at" json:"updated_at" form:"updated_at" `

}

func (Pendidikan) TableName() string {
	return "ref_pendidikan"
}
