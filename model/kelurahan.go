package model

import (
	// "time"
)

type Kelurahan struct {
	KelurahanId            int    	 `gorm:"column:kelurahan_id" json:"kelurahan_id" form:"kelurahan_id" `
	Kelurahan              string    `gorm:"column:kelurahan" json:"kelurahan" form:"kelurahan" `
	KecamatanId            int    	 `gorm:"column:kecamatan_id" json:"kecamatan_id" form:"kecamatan_id" `	
}

func (Kelurahan) TableName() string {
	return "kelurahan"
}