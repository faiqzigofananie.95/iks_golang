package model

import (
	// "time"
)

type Agama struct {
	Id       			 int    `gorm:"column:id" json:"id" form:"id" `
	Agama     			 string  `gorm:"column:agama" json:"agama" form:"agama" `
	CreatedAt            string `gorm:"column:created_at" json:"created_at" form:"created_at" `
	UpdatedAt            string `gorm:"column:updated_at" json:"updated_at" form:"updated_at" `
}

func (Agama) TableName() string {
	return "ref_agama"
}
