package model


type JSONResultSuccess struct {
    Error   string       `json:"error" example:"false"` 
    Message string       `json:"message"`
    Data    interface{}  `json:"data"`
}

type JSONResultError struct {
    Error   string       `json:"error" example:"true"` 
    Message string       `json:"message"`
}

type JSONInputSurvei struct {
	Provinsi  string    `json:"provinsi"`
}

type Data struct { 
    Provinsi  string    `json:"provinsi"`
    Tgl_akses string  	`json:"tgl akses"`
	Token	  int		`json:"token" example:"1234"`
}
