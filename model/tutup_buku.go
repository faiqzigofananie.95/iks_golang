package model

import (
	// "time"
)

type TutupBuku struct {
	TahunTutupBuku       int    `gorm:"column:tahun_tutup_buku" json:"tahun_tutup_buku" form:"tahun_tutup_buku" `
	TahunSelanjutnya     int    `gorm:"column:tahun_selanjutnya" json:"tahun_selanjutnya" form:"tahun_selanjutnya" `
	TglTutupBuku         string `gorm:"column:tanggal_tutup_buku" json:"tanggal_tutup_buku" form:"tanggal_tutup_buku" `
	Status               int    `gorm:"column:status" json:"status" form:"status" `
	StatusAktif          int    `gorm:"column:status_aktif" json:"status_aktif" form:"status_aktif" `
	StatusDelete         int    `gorm:"column:status_delete" json:"status_delete" form:"status_delete" `
	CreatedAt            string `gorm:"column:created_at" json:"created_at" form:"created_at" `
	UpdatedAt            string `gorm:"column:updated_at" json:"updated_at" form:"updated_at" `

}

func (TutupBuku) TableName() string {
	return "tutup_buku"
}
