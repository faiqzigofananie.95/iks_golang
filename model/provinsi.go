package model

import (
	// "time"
)

type Provinsi struct {
	ProvinsiId             int    	 `gorm:"column:provinsi_id" json:"provinsi_id" form:"provinsi_id" `
	Provinsi               string    `gorm:"column:provinsi" json:"provinsi" form:"provinsi" `
	Ctime              	   string 	 `gorm:"column:ctime" json:"ctime" form:"ctime"`
	Mtime              	   string 	 `gorm:"column:mtime" json:"mtime" form:"mtime"`
	Map_id                 string 	 `gorm:"column:map_id" json:"map_id" form:"map_id"`
}

func (Provinsi) TableName() string {
	return "provinsi"
}